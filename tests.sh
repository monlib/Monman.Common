#!/bin/bash

set -e

BUILD_OPTS="-s build_type=Debug"
BUILD_DIR=build
REFERENCE="Monman.Common/someversion@somedev/highly-experimental"

conan install --install-folder "${BUILD_DIR}" --build missing ${BUILD_OPTS} .
conan build --build-folder "${BUILD_DIR}" .
conan export-pkg --build-folder "${BUILD_DIR}" --force . "${REFERENCE}"
conan test --build missing ${BUILD_OPTS} test/ "${REFERENCE}"
