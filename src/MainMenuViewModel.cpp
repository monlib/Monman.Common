#include "monman/common/MainMenuViewModel.h"

#include <gsl/gsl>

#include "monman/common/SafeLua.h"

namespace monman::common {
    namespace {}

    MainMenuViewModel::MainMenuViewModel(std::shared_ptr<sol::state> lua,
        sol::table save,
        MonsterManagementFactory makeMonsterManagement) :
        _lua{std::move(lua)},
        _save{std::move(save)},
        _makeMonsterManagement{std::move(makeMonsterManagement)} {
        Ensures(_lua && _makeMonsterManagement);
    }

    std::optional<MonsterManagementViewModel>
    MainMenuViewModel::OpenMonsterManager() {
        Expects(_lua);
        auto tableOpt =
            MapResult<sol::table>(LuaSafeGet(*_lua, _save, "monsterStorage"));
        if (tableOpt.has_value()) {
            return _makeMonsterManagement->operator()(_lua, std::move(*tableOpt));
        } else {
            return std::nullopt;
        }
    }
}
