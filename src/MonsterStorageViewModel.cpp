#include "monman/common/MonsterStorageViewModel.h"

#include "monman/common/SafeLua.h"

namespace monman::common {
    namespace {
        int WrapAround(int upperBound, int index) {
            auto saneIndex = index - 1;
            auto saneResult = (saneIndex % upperBound + upperBound) % upperBound;
            auto stupidFinalResult = saneResult + 1;
            return stupidFinalResult;
        }
    }

    MonsterStorageViewModel::MonsterStorageViewModel(std::shared_ptr<sol::state> lua,
        sol::table storage,
        SharedMonsterDetailFactories monsterDetailFactories) :
        _onBoxChanged{nullptr},
        _lua{std::move(lua)},
        _monsterDetailFactories{std::move(monsterDetailFactories)},
        _storage{std::move(storage)},
        _currentIndex{1} {
        Expects(_lua && _storage);
        auto length = _storage.size();  // Probably crashes if __len fails
        auto boxOpt = [&]() -> std::optional<sol::table> {
            if (length >= 1) {
                return MapResult<sol::table>(LuaSafeGet(*_lua, _storage, 1));
            } else {
                return std::nullopt;
            }
        }();
        if (boxOpt.has_value()) {
            _currentBox = std::move(*boxOpt);
        } else {
            throw std::runtime_error("Could not open first box in monster storage!");
        }
    }

    void MonsterStorageViewModel::SetOnBoxChanged(
        std::function<void(int)> onBoxChanged) {
        _onBoxChanged = std::move(onBoxChanged);
    }

    void MonsterStorageViewModel::ChangeBoxBy(int change) {
        auto storageLength = gsl::narrow<int>(_storage.size());
        if (storageLength < 1) {
            throw std::runtime_error("Monster storage is empty!");
        }
        auto newIndex = WrapAround(storageLength, _currentIndex + change);
        auto newBox = MapResult<sol::table>(LuaSafeGet(*_lua, _storage, newIndex));
        if (!newBox.has_value()) {
            throw std::runtime_error("Could not load box!");
        }
        _currentIndex = newIndex;
        _currentBox = newBox.value();
        if (_onBoxChanged) {
            _onBoxChanged(change);
        }
    }

    std::optional<std::string> MonsterStorageViewModel::GetCurrentBoxName() {
        Expects(_lua);
        auto lookupResult = LuaSafeGet(*_lua, _currentBox, "name");
        return MapResult<std::string>(std::move(lookupResult));
    }

    std::optional<BriefMonInfo> MonsterStorageViewModel::GetBriefInfo(int index) {
        Expects(_lua && _currentBox);
        auto monOpt =
            MapResult<sol::table>(LuaSafeGet(*_lua, _currentBox, index + 1));
        if (!monOpt.has_value()) {
            return std::nullopt;
        }
        auto& mon = monOpt.value();
        bool isEgg =
            MapResult<bool>(LuaSafeGet(*_lua, mon, "isEgg")).value_or(false);
        if (isEgg) {
            return BriefMonInfo{0, 0, isEgg};
        } else {
            return BriefMonInfo{
                MapResult<int>(LuaSafeGet(*_lua, mon, "species")).value_or(0),
                MapResult<int>(LuaSafeGet(*_lua, mon, "form")).value_or(0),
                isEgg,
            };
        }
    }

    std::optional<std::unique_ptr<MonsterDetailViewModel>>
    MonsterStorageViewModel::GetDetailedInfo(int index) {
        auto monOpt =
            MapResult<sol::table>(LuaSafeGet(*_lua, _currentBox, index + 1));
        if (!monOpt.has_value()) {
            return std::nullopt;
        }
        auto& mon = *monOpt;
        auto binaryFormatsOpt =
            MapResult<sol::table>(LuaSafeGet(*_lua, mon, "binaryFormats"));
        if (!binaryFormatsOpt) {
            return nullptr;
        }
        auto& binaryFormats = *binaryFormatsOpt;
        for (int i = 0; i < gsl::narrow<int>(binaryFormats.size()); ++i) {
            auto formatOpt =
                MapResult<std::string>(LuaSafeGet(*_lua, binaryFormats, i + 1));
            if (formatOpt) {
                auto factoryIt = _monsterDetailFactories->find(*formatOpt);
                if (factoryIt != _monsterDetailFactories->end()) {
                    return (*factoryIt).second(mon);
                }
            }
        }
        return nullptr;
    }

    int MonsterStorageViewModel::GetCapacity() {
        Expects(_lua && _currentBox);
        constexpr auto UnreasonablyLargeCapacity = 5'000'000;
        return MapResult<int>(LuaSafeGet(*_lua, _currentBox, "capacity"))
            .value_or(UnreasonablyLargeCapacity);
    }

    sol::table MonsterStorageViewModel::GetStorage() { return _storage; }
    void MonsterStorageViewModel::SetStorage(sol::table storage) {
        auto newBox =
            MapResult<sol::table>(LuaSafeGet(*_lua, storage, _currentIndex));
        if (!newBox.has_value()) {
            throw std::runtime_error("Could not load box!");
        }
        _storage = storage;
        _currentBox = *newBox;
    }

    int MonsterStorageViewModel::GetBoxIndex() const { return _currentIndex - 1; }
}
