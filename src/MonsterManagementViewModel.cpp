#include "monman/common/MonsterManagementViewModel.h"

#include <gsl/gsl>

#include "monman/common/SafeLua.h"

namespace monman::common {
    namespace {}

    MonsterManagementViewModel::MonsterManagementViewModel(
        std::shared_ptr<sol::state> lua,
        sol::table primaryStorage,
        sol::table monswap,
        SharedMonsterDetailFactories monsterDetailFactories) :
        _lua{std::move(lua)},
        _primaryStorage{_lua, std::move(primaryStorage), monsterDetailFactories},
        _monsterDetailFactories{monsterDetailFactories},
        _monswap{std::move(monswap)} {
        Ensures(_lua && _monswap && _monsterDetailFactories);
    }

    gsl::not_null<MonsterStorageViewModel*>
    MonsterManagementViewModel::GetPrimaryStorage() {
        return &_primaryStorage;
    }

    [[nodiscard]] bool MonsterManagementViewModel::SwapMonsters(
        MonPosition lhs, MonPosition rhs) {
        Expects(lhs.positions.size() == rhs.positions.size());
        if (std::tie(lhs.storage, lhs.box) == std::tie(rhs.storage, rhs.box)) {
            sol::table storage = GetStorage(lhs.storage).GetStorage();
            sol::protected_function swap = _monswap["swapSameBox"];
            auto storageOpt = MapResult<sol::table>(swap(storage,
                lhs.box + 1,
                sol::as_table(lhs.positions),
                sol::as_table(rhs.positions)));
            if (storageOpt) {
                GetStorage(lhs.storage).SetStorage(*storageOpt);
                return true;
            } else {
                return false;
            }

        } else if (lhs.storage == rhs.storage) {
            sol::table storage = GetStorage(lhs.storage).GetStorage();
            sol::protected_function swap = _monswap["swapSameStorage"];
            auto storageOpt = MapResult<sol::table>(swap(storage,
                lhs.box + 1,
                sol::as_table(lhs.positions),
                rhs.box + 1,
                sol::as_table(rhs.positions)));
            if (storageOpt) {
                GetStorage(lhs.storage).SetStorage(*storageOpt);
                return true;
            } else {
                return false;
            }

        } else {
            sol::table lStorage = GetStorage(lhs.storage).GetStorage();
            sol::table rStorage = GetStorage(rhs.storage).GetStorage();
            sol::protected_function swap = _monswap["swap"];
            sol::protected_function_result result = swap(lStorage,
                lhs.box + 1,
                sol::as_table(lhs.positions),
                rStorage,
                rhs.box + 1,
                sol::as_table(rhs.positions));
            auto storageOpt = MapResult<std::tuple<sol::table, sol::table>>(result);
            if (storageOpt) {
                auto& [newL, newR] = *storageOpt;
                GetStorage(lhs.storage).SetStorage(newL);
                GetStorage(rhs.storage).SetStorage(newR);
                return true;
            } else {
                return false;
            }
        }
    }

    MonsterStorageViewModel& MonsterManagementViewModel::GetStorage(Storage s) {
        switch (s) {
        case Storage::Primary: return _primaryStorage;
        default: Expects(false);
        }
    }

    MonsterManagementViewModel MonsterManagementFactoryImpl::operator()(
        std::shared_ptr<sol::state> state, sol::table primaryStorage) {
        return MonsterManagementViewModel{std::move(state),
            std::move(primaryStorage),
            _monswap,
            _monsterDetailFactories};
    }

    void MonsterManagementFactoryImpl::registerImplicitMigration(
        std::string_view from,
        std::string_view to,
        sol::function convertTo,
        sol::function convertBack) {
        sol::function addMigration = _monswap["addMigration"];
        addMigration(from, to, convertTo, convertBack);
    }

    MonsterManagementFactoryImpl::MonsterManagementFactoryImpl(sol::state_view lua,
        const char* filename,
        SharedMonsterDetailFactories monsterDetailFactories) :
        _monswap{lua.script_file(filename).get<sol::table>()},
        _monsterDetailFactories{monsterDetailFactories} {
        Ensures(_monswap && _monsterDetailFactories);
    }
}
