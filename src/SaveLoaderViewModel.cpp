#include "monman/common/SaveLoaderViewModel.h"

#include <gsl/gsl>

namespace monman::common {
    SaveLoaderViewModel::SaveLoaderViewModel(
        std::shared_ptr<sol::state> lua, sol::protected_function loader) :
        _lua{lua},
        _loader{loader} {
        Expects(lua && loader);
    }

    LoadedSave SaveLoaderViewModel::OpenSave(const char* filename) {
        sol::protected_function_result result = _loader(filename);
        if (result.valid() && sol::object{result}.is<sol::table>()) {
            sol::table save = result;
            return LoadedSave{save};
        } else {
            throw std::runtime_error("Could not load save!");
        }
    }
}
