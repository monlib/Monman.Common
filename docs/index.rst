.. Monman.Common documentation master file, created by
   sphinx-quickstart on Tue Jul 31 17:31:54 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Monman.Common's documentation!
=========================================

.. doxygenclass:: monman::common::Application
    :members:

.. doxygenclass:: monman::common::SaveLoaderViewModel
    :members:

.. doxygenclass:: monman::common::MainMenuViewModel
    :members:

.. doxygenenum:: monman::common::Storage

.. doxygenstruct:: monman::common::MonPosition
    :members:
    :undoc-members:

.. doxygenclass:: monman::common::MonsterManagementViewModel
    :members:

.. doxygenclass:: monman::common::MonsterStorageViewModel
    :members:

.. doxygenstruct:: monman::common::BriefMonInfo
    :members:
    :undoc-members:

.. doxygenstruct:: monman::common::MonsterDetailViewModel

.. doxygentypedef:: monman::common::MonsterDetailFactory

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
