# Monman.Common

## Building the docs

Requires Breathe, Doxygen, and Sphinx. The output is in `_build/index.html`.

```sh
cd docs/ && doxygen && sphinx-build . _build
```

