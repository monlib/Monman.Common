#include "catch.hpp"

#include "monman/common/MainMenuViewModel.h"

using namespace monman::common;

namespace {
    MainMenuViewModel CreateMainMenuWith(std::string script) {
        auto lua = std::make_shared<sol::state>();
        OpenAllLuaLibraries(*lua);
        auto save = lua->safe_script(script);
        auto makeMonsterManagement = std::make_shared<MonsterManagementFactoryImpl>(
            *lua, "monswap.lua", std::make_shared<MonsterDetailFactories>());
        return MainMenuViewModel{
            std::move(lua), std::move(save), std::move(makeMonsterManagement)};
    }
}

SCENARIO("A main menu lets the user select functionality") {
    GIVEN("A main menu for a save without monster storage") {
        auto menu = CreateMainMenuWith(R"(return { monsterStorage = nil, })");
        WHEN("The monster storage is requested") {
            auto storage = menu.OpenMonsterManager();
            THEN("nullopt is returned") { REQUIRE(storage == std::nullopt); }
        }
    }

    GIVEN("A main menu for a save where monsterStorage errors") {
        auto menu = CreateMainMenuWith(R"(
            local t = { __index = function(self, k) error(k) end, }
            return setmetatable(t, t))");
        WHEN("The monster storage is requested") {
            auto storage = menu.OpenMonsterManager();
            THEN("nullopt is returned") { REQUIRE(storage == std::nullopt); }
        }
    }

    GIVEN("A main menu for a save where monsterStorage has the wrong type") {
        auto menu = CreateMainMenuWith(R"(return { monsterStorage = 5, })");
        WHEN("The monster storage is requested") {
            auto storage = menu.OpenMonsterManager();
            THEN("nullopt is returned") { REQUIRE(storage == std::nullopt); }
        }
    }

    GIVEN("A main menu for a save where monsterStorage exists") {
        auto menu =
            CreateMainMenuWith(R"(return { monsterStorage = { [1] = {}, }, })");
        WHEN("The monster storage is requested") {
            std::optional<MonsterManagementViewModel> storage =
                menu.OpenMonsterManager();
            THEN("One is returned") { REQUIRE(storage.has_value()); }
        }
    }
}
