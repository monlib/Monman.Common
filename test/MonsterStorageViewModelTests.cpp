#include <limits>

#include "catch.hpp"

#include "monman/common/MonsterStorageViewModel.h"

using namespace monman::common;

MonsterStorageViewModel CreateMonsterStorageViewModelWith(std::string code) {
    auto lua = std::make_shared<sol::state>();
    OpenAllLuaLibraries(*lua);
    sol::table storage = lua->script(code);
    return MonsterStorageViewModel{
        lua, storage, std::make_shared<MonsterDetailFactories>()};
}

SCENARIO("A MonsterStorageVM provide access to a MonsterStorage object") {
    GIVEN("A VM for a MonsterStorage object with two named boxes") {
        auto storage = CreateMonsterStorageViewModelWith(
            R"(return { [1] = { name = "First", }, [2] = { name = "Second", }, })");

        WHEN("A box change handler is set and the box is changed") {
            bool boxChanged = false;
            auto onBoxChanged = [&boxChanged](auto) { boxChanged = true; };
            storage.SetOnBoxChanged(onBoxChanged);
            storage.ChangeBoxBy(1);
            THEN("It is invoked") { REQUIRE(boxChanged); }
        }

        WHEN("No box change handler is set") {
            storage.SetOnBoxChanged(nullptr);
            THEN("The box can still be changed") { storage.ChangeBoxBy(1); }
        }

        WHEN("The next box is selected") {
            storage.ChangeBoxBy(1);
            THEN("The current box's name is correct") {
                auto name = storage.GetCurrentBoxName();
                REQUIRE(name.has_value());
                REQUIRE(name.value() == "Second");
            }

            AND_WHEN("the next box is selected again") {
                storage.ChangeBoxBy(1);
                THEN("The selection safely rolls over and the current box is the "
                     "first") {
                    auto name = storage.GetCurrentBoxName();
                    REQUIRE(name.has_value());
                    REQUIRE(name.value() == "First");
                }
            }

            AND_WHEN("the previous box is selected afterwards") {
                storage.ChangeBoxBy(-1);
                THEN("The first box is selected again and its name is returned") {
                    auto name = storage.GetCurrentBoxName();
                    REQUIRE(name.has_value());
                    REQUIRE(name.value() == "First");
                }
            }
        }
    }

    GIVEN("A VM for a MonsterStorage with a named box") {
        auto storage = CreateMonsterStorageViewModelWith(
            R"(return { [1] = { name = "Foo", }, })");

        WHEN("The current box's name is requested") {
            auto name = storage.GetCurrentBoxName();
            THEN("It matches") {
                REQUIRE(name.has_value());
                REQUIRE(name.value() == "Foo");
            }
        }
    }

    GIVEN("A VM for a MonsterStorage with six Monsters in a Box") {
        auto storage = CreateMonsterStorageViewModelWith(
            R"(return { {
                [1] = { species = 1, form = 100, isEgg = False, },
                [2] = { species = 2, isEgg = False, },
                [3] = { species = 3, },
                [4] = { species = 4, isEgg = True, },
                [5] = { isEgg = True, },
                [6] = { species = 6, form = 600, },
            }, })");
        WHEN("Brief info for a monster with species, form, and isEgg (=False) is "
             "requested") {
            auto info = storage.GetBriefInfo(0);
            THEN("The returned struct contains the correct values") {
                REQUIRE(info.has_value());
                CHECK(info->species == 1);
                CHECK(info->form == 100);
                CHECK(!info->isEgg);
            }
        }
        WHEN("Brief info for a monster without a form, but with species and "
             "isEgg(=False) set is requested") {
            auto info = storage.GetBriefInfo(1);
            THEN("All properties are correct and form is defaulted to zero") {
                REQUIRE(info.has_value());
                CHECK(info->species == 2);
                CHECK(info->form == 0);
                CHECK(!info->isEgg);
            }
        }
        WHEN("Brief info with only a species is requested") {
            auto info = storage.GetBriefInfo(2);
            THEN("Species is correct and form and isEgg are defaulted to 0/False") {
                REQUIRE(info.has_value());
                CHECK(info->species == 3);
                CHECK(info->form == 0);
                CHECK(!info->isEgg);
            }
        }
        WHEN("Brief info for an egg with a species is requested") {
            auto info = storage.GetBriefInfo(3);
            THEN("isEgg is true and all other fields are unspecified") {
                REQUIRE(info.has_value());
                CHECK(!info->isEgg);
            }
        }
        WHEN("Brief info for an egg without a species is requested") {
            auto info = storage.GetBriefInfo(4);
            THEN("isEgg is true and all other fields are unspecified") {
                REQUIRE(info.has_value());
                CHECK(!info->isEgg);
            }
        }
        WHEN("Brief info for a monster with a species and form, but no isEgg flag "
             "is requested") {
            auto info = storage.GetBriefInfo(5);
            THEN(
                "isEgg is defaulted to false and all other properties are correct") {
                REQUIRE(info.has_value());
                CHECK(info->species == 6);
                CHECK(info->form == 600);
                CHECK(!info->isEgg);
            }
        }
        WHEN("Brief info for a non-existent monster is requested") {
            auto info = storage.GetBriefInfo(6);
            THEN("nullopt is returned") { REQUIRE(info == std::nullopt); }
        }
    }

    GIVEN("A VM for a MonsterStorage without a specified capacity") {
        auto storage = CreateMonsterStorageViewModelWith(R"(
            return { [1] = { capacity = function() end, }, }
        )");
        WHEN("Capacity is requested") {
            auto capacity = storage.GetCapacity();
            THEN("It is set to a large value, that doesn't risk signed overflow in "
                 "normal circumstances.") {
                REQUIRE(capacity >= 1'000'000);
            }
        }
    }

    GIVEN("A VM with a capacity") {
        auto storage = CreateMonsterStorageViewModelWith(R"(
            return { [1] = { capacity = 1234, }, }
        )");
        WHEN("Its capacity is requested") {
            auto capacity = storage.GetCapacity();
            THEN("The correct value is returned") { REQUIRE(capacity == 1234); }
        }
    }
}
