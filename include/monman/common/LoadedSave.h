#ifndef MONMAN_COMMON_LOADEDSAVE_H
#define MONMAN_COMMON_LOADEDSAVE_H

#include "monman/common/sol.h"

namespace monman::common {
    struct LoadedSave {
        sol::table obj;
    };
}

#endif
