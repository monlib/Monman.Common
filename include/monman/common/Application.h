#ifndef MONMAN_COMMON_APPLICATION_H
#define MONMAN_COMMON_APPLICATION_H

#include <map>
#include <memory>
#include <string>

#include "lua.hpp"

#include "monman/common/MainMenuViewModel.h"
#include "monman/common/MonsterDetailViewModel.h"
#include "monman/common/SaveLoaderViewModel.h"
#include "monman/common/sol.h"

namespace monman::common {
    /// Entry point for all applications.
    class Application {
    public:
        /// Constructs an Application
        Application(const char* monswapPath);

        /// Creates an object to load save files
        SaveLoaderViewModel getSaveLoader();

        /// Registers a function as a lua module
        void registerLuaModule(const std::string& key, ::lua_CFunction luaopen);

        /** \brief Registers the functions to load saves
         *
         * The loader functions are lazily loaded once getSaveLoader is called.
         * The returned protected_function should be a lua function from a filename
         * to either a Monlib.Core Save object or nil.
         */
        void registerLoaders(
            std::function<sol::protected_function(sol::state_view)>);

        /// Registers an implicit migration to be used when moving mons. The first
        /// two arguments must be binary format tags.
        void registerImplicitMigration(std::string_view from,
            std::string_view to,
            sol::function migrateTo,
            sol::function migrateBack);

        /// Provides access to the underlying lua instance.
        sol::state_view getLua();

        /// Opens the main menu using a previosuly loaded save
        MainMenuViewModel openMainMenu(LoadedSave);

        /// Register a MonsterDetailViewModel factory for the given binary format.
        void registerMonsterDetailViewModel(std::string_view, MonsterDetailFactory);

    private:
        std::shared_ptr<sol::state> _lua;
        std::function<sol::protected_function(sol::state_view)> _getLoaders;
        SharedMonsterDetailFactories _monsterDetailFactories;
        MonsterManagementFactory _makeMonsterManagement;
    };
}

#endif
