#ifndef MONMAN_COMMON_MAINMENUVIEWMODEL_H
#define MONMAN_COMMON_MAINMENUVIEWMODEL_H

#include <optional>

#include "monman/common/LoadedSave.h"
#include "monman/common/MonsterManagementViewModel.h"

namespace monman::common {
    /// Provides a main menu
    class MainMenuViewModel {
    public:
        MainMenuViewModel(
            std::shared_ptr<sol::state>, sol::table, MonsterManagementFactory);

        /// Produces the view model for editing monsters
        std::optional<MonsterManagementViewModel> OpenMonsterManager();

    private:
        std::shared_ptr<sol::state> _lua;
        sol::table _save;
        MonsterManagementFactory _makeMonsterManagement;
    };
}

#endif
