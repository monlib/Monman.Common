#ifndef MONMAN_COMMON_MONSTERSTORAGEVIEWMODEL_H
#define MONMAN_COMMON_MONSTERSTORAGEVIEWMODEL_H

#include <functional>
#include <map>
#include <optional>
#include <string>

#include "monman/common/MonsterDetailViewModel.h"
#include "monman/common/sol.h"

namespace monman::common {
    /// Provides summarized info for a single monster.
    struct BriefMonInfo {
        int species;
        int form;
        bool isEgg;
    };

    /// Provides access to a set of boxes, one at a time.
    class MonsterStorageViewModel {
    public:
        MonsterStorageViewModel(
            std::shared_ptr<sol::state>, sol::table, SharedMonsterDetailFactories);

        /// Set the function to call when the box is changed.
        void SetOnBoxChanged(std::function<void(int)>);

        /// Change the current box by the specified number (Wraps around).
        void ChangeBoxBy(int);

        /// Returns the current box's name if present.
        std::optional<std::string> GetCurrentBoxName();

        /// Returns summarized information for the given monster.
        std::optional<BriefMonInfo> GetBriefInfo(int index);

        /// Returns a MonsterDetailViewModel implementation if the monster has a
        /// fitting binaryFormat. Returns nullopt for empty positions, nullptr for
        /// no known MonsterDetailViewModel and a pointer to a derived class
        /// otherwise.
        std::optional<std::unique_ptr<MonsterDetailViewModel>> GetDetailedInfo(
            int index);

        /// Returns the capacity of the current box.
        int GetCapacity();

        /// Returns the index of the current box.
        int GetBoxIndex() const;

        sol::table GetStorage();
        void SetStorage(sol::table);  // Does not raise OnBoxChanged

    private:
        std::function<void(int)> _onBoxChanged;
        std::shared_ptr<sol::state> _lua;
        SharedMonsterDetailFactories _monsterDetailFactories;
        sol::table _storage;
        sol::table _currentBox;
        int _currentIndex;
    };
}

#endif
