#ifndef MONMAN_COMMON_SAVELOADERVIEWMODEL_H
#define MONMAN_COMMON_SAVELOADERVIEWMODEL_H

#include <string>

#include "monman/common/LoadedSave.h"
#include "monman/common/sol.h"

namespace monman::common {
    /// Loads Save objects
    class SaveLoaderViewModel {
    public:
        SaveLoaderViewModel(
            std::shared_ptr<sol::state> lua, sol::protected_function loader);

        /// Opens the save file at the given path
        LoadedSave OpenSave(const char*);

    private:
        std::shared_ptr<sol::state> _lua;
        sol::protected_function _loader;
    };
}

#endif
