#ifndef MONMAN_COMMON_MONSTERMANAGEMENTVIEWMODEL_H
#define MONMAN_COMMON_MONSTERMANAGEMENTVIEWMODEL_H

#include <memory>

#include <gsl/gsl>

#include "monman/common/MonsterStorageViewModel.h"
#include "monman/common/sol.h"

namespace monman::common {
    /// Distinguishes between a MonsterManagementViewModel's storage objects
    enum class Storage { Primary };

    /// Describes a monsters position within a group of storage objects
    struct MonPosition {
        Storage storage;
        int box;
        gsl::span<int> positions;
    };

    /// Manages monster storage
    class MonsterManagementViewModel {
    public:
        MonsterManagementViewModel(std::shared_ptr<sol::state>,
            sol::table primaryStorage,
            sol::table monswap,
            SharedMonsterDetailFactories);

        /// Returns a view model for the primary monster storage system.
        gsl::not_null<MonsterStorageViewModel*> GetPrimaryStorage();

        /// Attempts to swap the monsters at the given positions. Returns true if
        /// successful. Both arguments must have the same number of positions.
        [[nodiscard]] bool SwapMonsters(MonPosition, MonPosition);

    private:
        std::shared_ptr<sol::state> _lua;
        MonsterStorageViewModel _primaryStorage;
        SharedMonsterDetailFactories _monsterDetailFactories;
        sol::table _monswap;

        MonsterStorageViewModel& GetStorage(Storage);
    };

    class MonsterManagementFactoryImpl {
    public:
        MonsterManagementFactoryImpl(
            sol::state_view, const char*, SharedMonsterDetailFactories);
        MonsterManagementViewModel operator()(
            std::shared_ptr<sol::state> state, sol::table primaryStorage);
        void registerImplicitMigration(
            std::string_view, std::string_view, sol::function, sol::function);

    private:
        sol::table _monswap;
        SharedMonsterDetailFactories _monsterDetailFactories;
    };

    using MonsterManagementFactory = std::shared_ptr<MonsterManagementFactoryImpl>;
}

#endif
