from conans import ConanFile, CMake

class MonmanCommon(ConanFile):
    name = 'Monman.Common'
    license = 'GPLv3+'
    url = 'https://gitlab.com/monlib/Monlib.Core.git'
    description = ''
    settings = 'os', 'arch', 'build_type', 'compiler'
    exports_sources = 'src/*', 'include/*', 'lua/*', 'CMakeLists.txt'
    generators = 'cmake'
    build_requires = (
        'cmake_installer/[~3.10]@conan/stable',
    )
    requires = (
        'lua/[~5.3.4]@monlib/stable',
        'sol2/2.20.4@monlib/stable',
        'gsl_microsoft/20180102@bincrafters/stable',
    )

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy('*.h', dst='include', src='include')
        self.copy('*monman-common.lib', dst='lib', keep_path=False)
        self.copy('*.lua', src='lua', dst='lua', keep_path=True)
        self.copy('*.dll', dst='lib', keep_path=False)
        self.copy('*.so', dst='lib', keep_path=False)
        self.copy('*.dylib', dst='lib', keep_path=False)
        self.copy('*.a', dst='lib', keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ['monman-common']
        self.cpp_info.defines = ['GSL_THROW_ON_CONTRACT_VIOLATION']
